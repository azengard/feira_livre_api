# README #

## Feira Livre API ##

* Version 1.0

This project is an API for register all 'Feiras Livres' in a csv file and perform
search, create, update and exclude a 'feira'.

The source of the csv file used can be found here:
http://www.prefeitura.sp.gov.br/cidade/secretarias/upload/chamadas/feiras_livres_1429113213.zip

For execute the API calls and documentation was used the HttPie, they can be found here:
https://httpie.org/ 

The DataBase used was PostgresSql.

### How do I get set up? ###

* Summary of set up


* Configuration

Choose a directory to work and clone this repository:
```console
git clone git@bitbucket.org:azengard/feira_livre_api.git
```

or:
```console
git clone https://azengard@bitbucket.org/azengard/feira_livre_api.git
```

Enter on it:
```console
cd feira_livre_api
```

Create a Virtual Enviroment (Highly recommended):
```console
python -m venv .feiras

source .feiras/bin/activate
```

* Dependencies

Install the API Dependencies and prepare the .env file:
```console
pip install -r requirements.txt

cp instance/env_sample .env
```

Now you need config the Enviroments Variables editing the .env file where:

SECRET: Is your secret key for this api, change it for something really secure.

APP_SETTINGS: You can choose what setting use, the options are: 'development', 'testing', 'staging' and 'production'.

DATABASE_URL and TEST_DATABASE_URL: Here you pass the name and password of you database and a name for the application and the test database.

* Database configuration

If using Postgres create the database with the same name you set in DATABASE_URL and TEST_DATABASE_URL:
```console
createdb <name_table_aplication>

createdb <name_table_test>
```
Now create the tables.

```console
python manage.py db upgrade
```

* How upload a csv file

For exporting a csv file to populate the Database run the command:
```console
python export_csv.py <path_to_csv_file/file.csv> 
```

You can find a valid csv file in: data/DEINFO_AB_FEIRASLIVRES_2014.csv.

* How to run tests

You can run the tests after config and create de Test Database, with the command:
```console
python test_feira.py
```

For see a report with the code test coverage, use:
```console
coverage run run.py

"ctrl + C"

coverage report
```

* Deployment instructions

With the .env properly configured run the command:
```console
python run.py
```

### How do I use the API? ###

There are two endpoints for this API: `/feiras` and `/feiras/<registro>`

## `/feiras/`

Method `GET`:
Here you can list all the 'feiras' in the database.

Response code: 200 OK.

```console
http GET localhost:5000/feiras/

or 

http localhost:5000/feiras/
```

You can also filter your search, need pass the GET atribute otherwise HttPie will made a POST.

```console
http GET localhost:5000/feiras/ regiao5=Leste
```

You can apply more than a filter at once!
```console
http GET localhost:5000/feiras/ regiao5=Leste distrito="SAO MATEUS" 
```

Method `POST`:

For Create a new 'feira' use this method along the data for the new 'feira'.

'registro' is a required field and need be unique.

For sending a number with the HttpPie use ':='. 

Response code: 201 CREATED.

```console
http POST localhost:5000/feiras/ long=-46550164 lat=-23558733 setcens:=355030885000091 regiao5=Leste referencia='TV RUA PRETORIA' registro=4041-1

```

List of all fields and type:

* registro = String, tamanho 6, REQUIRED
* setcens = Inteiro
* areap = Inteiro
* coddist = Inteiro
* codsubpref = Inteiro
* long = String, tamanho 9
* lat = String, tamanho 9
* distrito = String, tamanho 50
* subprefe = String, tamanho 50
* regiao5 = String, tamanho 20
* regiao8 = String, tamanho 20
* nome_feira = String, tamanho 30
* logradouro = String, tamanho 50
* numero = String, tamanho 10
* bairro = String, tamanho 50
* referencia = String, tamanho 50


## `/feiras/<registro>`

Method `GET`:

For get the details of one 'feira':

If find it:

Response code: 200 OK.

If not find it:

Response code: 404 NOT_FOUND.

```console
http GET localhost:5000/feiras/4041-1

or

http localhost:5000/feiras/4041-1
```

Method `PUT`:

For update one 'feira' use this method with the register value and the new data.

Response code: 200 OK.

```console
http PUT localhost:5000/feiras/4041-1 referencia='Rua Joaquim Mendes'
```

Method `DELETE`:

For delete a 'feira' use this method with the register value.

Response code: 204 NO_CONTENT.

```console
http DELETE localhost:5000/feiras/4041-1
```


### Who do I talk to? ###

* Repo owner **Carlos Henrique**
