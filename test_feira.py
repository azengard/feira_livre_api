import os, json, unittest
from app import create_app
from app.db import db
from export_csv import export_csv_to_database
from decouple import config


class FeiraLivreTestCase(unittest.TestCase):
    """
    This class represents the feira test case.
    """

    def setUp(self):
        """
        Define test variables and initialize app
        and binds the app to the current context.
        """
        self.app = create_app(config_name=config('APP_TEST_SETTINGS'))
        self.client = self.app.test_client
        self.feira = dict(
            long = '-46550164',
            lat = '-23558733',
            setcens = 355030885000091,
            areap = 3550308005040,
            coddist = 87,
            distrito = 'VILA FORMOSA',
            codsubpref = 26,
            subprefe = 'ARICANDUVA-FORMOSA-CARRAO',
            regiao5 = 'Leste',
            regiao8 = 'Leste 1',
            nome_feira = 'VILA FORMOSA',
            registro = '4041-0',
            logradouro = 'RUA MARAGOJIPE',
            numero = 'S/N',
            bairro = 'VL FORMOSA',
            referencia = 'TV RUA PRETORIA'
        )

        self.feira_put = {
            'distrito': 'MOOCA',
            'regiao5': 'Sul',
            'registro': '4041-0',
            'referencia': 'TV RUA DOS TRILHOS',
        }

        self.invalid = '0000-0'

        # Create de DB before post data.
        with self.app.app_context():
            db.create_all()

        self.feira_obj = self.client().post(
            '/feiras/', data=json.dumps(self.feira),
            content_type='application/json'
        )

    def test_create_feira_success(self):
        """
        Test API can create a feira (POST request).
        """
        self.feira['registro']='4041-1'
        feira_obj = self.client().post(
            '/feiras/', data=json.dumps(self.feira),
            content_type='application/json'
        )
        
        self.assertEqual(feira_obj.status_code, 201)
        self.assertIn(self.feira['registro'], str(feira_obj.data))

    def test_create_feira_fail(self):
        """
        Test API don't create a feira with no register value. (POST request).
        """
        self.feira.pop('registro')
        feira_obj = self.client().post(
            '/feiras/', data=json.dumps(self.feira),
            content_type='application/json'
        )
        
        self.assertEqual(feira_obj.status_code, 400)

    def test_get_all_feiras(self):
        """
        Test API can get a feira (GET request).
        """
        feira_obj = self.client().get('/feiras/')

        self.assertEqual(feira_obj.status_code, 200)
        self.assertIn('4041-0', str(feira_obj.data))

    def test_get_one_feira(self):
        """
        Test API can get a single feira by using it's registro.
        """
        feira_obj = self.client().get(f"/feiras/{self.feira['registro']}")

        self.assertEqual(feira_obj.status_code, 200)
        self.assertIn(self.feira['registro'], str(feira_obj.data))

    def test_edit_feira_valid(self):
        """
        Test API can edit an existing feira. (PUT request).
        """
        feira_obj_put = self.client().put(
            f"/feiras/{self.feira['registro']}",
            data=self.feira_put
        )

        self.assertEqual(feira_obj_put.status_code, 200)
        self.assertIn(self.feira_put['distrito'], str(feira_obj_put.data))

    def test_edit_feira_invalid(self):
        """
        Test API don't edit an inexisting feira. (PUT request).
        """
        feira_obj_put = self.client().put(
            f"/feiras/{self.invalid}",
            data=self.feira_put
        )

        self.assertEqual(feira_obj_put.status_code, 204)

    def test_delete_feira_success(self):
        """
        Test API can delete an existing feira. (DELETE request).
        """
        feira_obj_del = self.client().delete(f"/feiras/{self.feira['registro']}")

        self.assertEqual(feira_obj_del.status_code, 204)


    def test_delete_feira_fail(self):
        """
        Test API cannot delete an inexisting feira. (DELETE request).
        """
        invalid_obj = self.client().delete(f"/feiras/{self.invalid}")
        
        self.assertEqual(invalid_obj.status_code, 404)

    def test_export_csv(self):
        """
        Test API can export a csv. (DELETE request).
        """
        # Need delete the object create in the setUp to count correctly.
        self.client().delete(f"/feiras/{self.feira['registro']}")

        export_csv_to_database('data/TEST_FEIRA.csv', test=True)

        from app.models import FeiraLivre
        feira_obj = FeiraLivre().query.count()

        self.assertEqual(feira_obj, 10)

    def tearDown(self):
        """
        Teardown all initialized variables.
        """
        with self.app.app_context():
            db.session.remove()
            db.drop_all()


if __name__ == "__main__":
    unittest.main()