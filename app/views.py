import json
from flask import Blueprint, request, jsonify, abort
from app.db import db
from app.models import FeiraLivre
from app.helpers import validate_data_keys


feira_blueprint = Blueprint('feira_livre', __name__)


@feira_blueprint.route('/feiras/', methods=['GET'])
def list_feiras():
    """
    Return a List with all Feiras objects.
    If receive data will perform a filter instead.
    """

    data_filter = validate_data_keys(request.data)
    if data_filter:
        feiras = FeiraLivre.query.filter_by(**data_filter)
    else:
        feiras = FeiraLivre.get_all()
    
    response = []
    for feira in feiras:
        obj = feira.as_dict()
        response.append(obj)

    return jsonify(response), 200


@feira_blueprint.route('/feiras/', methods=['POST'])
def post_feira():
    """
    Create a new Feira object.
    """

    registro = str(request.data.get('registro', ''))
    if not registro:
        return {"message": "A Feira need a valid 'registro' value."}, 400

    feira_exists = FeiraLivre.query.filter_by(registro=registro).first()
    if feira_exists:
        return {"message": "This 'registro' is already in use by another Feira."}, 400

    try:
        feira = FeiraLivre(**request.data)
        feira.save()
        return jsonify(feira.as_dict()), 201

    except Exception as e:
        return {"message": "Invalid input syntax."}, 400


@feira_blueprint.route('/feiras/<string:registro>', methods=['GET'])
def get_feira(registro):
    """
    Get a Feira object.
    """

    feira = FeiraLivre.query.filter_by(registro=registro).first()
    if not feira:
        return {"message": f"Feira {registro} not found."}, 404

    return jsonify(feira.as_dict()), 200


@feira_blueprint.route('/feiras/<string:registro>', methods=['PUT'])
def put_feira(registro):
    """
    Update a Feira object.
    """

    feira = FeiraLivre.query.filter_by(registro=registro).first()
    if not feira:
        return '', 204

    request.data.pop('registro', None)
    request.data.pop('id', None)

    try:
        for key, value in request.data.items():
            setattr(feira, key, value)
        feira.save()
        return jsonify(feira.as_dict()), 200

    except Exception as e:
        return {"message": "Invalid input syntax."}, 400


@feira_blueprint.route('/feiras/<string:registro>', methods=['DELETE'])
def delete_feira(registro):
    """
    Delete a Feira object.
    """

    feira = FeiraLivre.query.filter_by(registro=registro).first()
    if not feira:
        return {"message": f"Feira {registro} not found"}, 404
    
    feira.delete()
    return '', 204
