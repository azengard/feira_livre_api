from app.models import FeiraLivre


def validate_data_keys(data):
    """
    Receives a dict with all request.data and verify
    if the keys in this data exists in the table.
    Return only the items present in both items.

    param: data: dict{'A':'aa', 'B':'bb'}
    param: table_keys: list['A', 'C']
    return: data_filter: dict{'A':'aa'} 
    """
    table_keys = FeiraLivre.__table__.columns.keys()
    common_keys = set(data).intersection(table_keys)
    
    data_filter = dict()
    for key in common_keys:
        data_filter[key] = data[key]
    return data_filter