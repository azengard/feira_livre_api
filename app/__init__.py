from flask_api import FlaskAPI
from instance.config import app_config
from app.logger import set_log
from app.views import feira_blueprint
from app.db import db


def create_app(config_name):
    app = FlaskAPI(__name__, instance_relative_config=True)
    app.config.from_object(app_config[config_name])
    app.config.from_pyfile('config.py')

    app.register_blueprint(feira_blueprint)

    set_log(app)

    db.init_app(app)

    return app