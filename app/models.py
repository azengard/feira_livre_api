from app.db import db


class FeiraLivre(db.Model):
    """
    This class represents the feiras_livres table.
    """
    __tablename__ = "feiras_livres"

    id = db.Column(db.Integer, primary_key=True)
    long = db.Column(db.String(9))
    lat = db.Column(db.String(9))
    setcens = db.Column(db.BigInteger)
    areap = db.Column(db.BigInteger)
    coddist = db.Column(db.Integer)
    distrito = db.Column(db.String(50))
    codsubpref = db.Column(db.Integer)
    subprefe = db.Column(db.String(50))
    regiao5 = db.Column(db.String(20))
    regiao8 = db.Column(db.String(20))
    nome_feira = db.Column(db.String(30))
    registro = db.Column(db.String(6))
    logradouro = db.Column(db.String(50))
    numero = db.Column(db.String(10))
    bairro = db.Column(db.String(50))
    referencia = db.Column(db.String(50))

    def __init__(self, **kwargs):
        self.long = kwargs.get("long", None)
        self.lat = kwargs.get("lat", None)
        self.setcens = kwargs.get("setcens", None)
        self.areap = kwargs.get("areap", None)
        self.coddist = kwargs.get("coddist", None)
        self.distrito = kwargs.get("distrito", None)
        self.codsubpref = kwargs.get("codsubpref", None)
        self.subprefe = kwargs.get("subprefe", None)
        self.regiao5 = kwargs.get("regiao5", None)
        self.regiao8 = kwargs.get("regiao8", None)
        self.nome_feira = kwargs.get("nome_feira", None)
        self.registro = kwargs.get("registro", None)
        self.logradouro = kwargs.get("logradouro", None)
        self.numero = kwargs.get("numero", None)
        self.bairro = kwargs.get("bairro", None)
        self.referencia = kwargs.get("referencia", None)  

    def save(self):
        db.session.add(self)
        db.session.commit()

    @staticmethod
    def get_all():
        return FeiraLivre.query.all()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def as_dict(self):
       return {col.name: getattr(self, col.name) for col in self.__table__.columns}
       
    def __repr__(self):
        return '<app.model.FeiraLivre {}>'.format(self.id)
