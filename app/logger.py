import logging, os
from decouple import config
from logging.handlers import RotatingFileHandler


def set_log(app):
    """
    Build a log object from a app.
    And save a .log file.
    """
    logger = logging.getLogger('werkzeug')
    
    log_handler = RotatingFileHandler(
        config('LOG_FOLDER_NAME', default='logs/api_feira.log'),
        maxBytes=config('LOG_ROTATE_SIZE_MB', default=1, cast=int) * 1024 ** 2,
        backupCount=config('LOG_ROTATE_COUNT', default=3, cast=int))

    logger.addHandler(log_handler)
