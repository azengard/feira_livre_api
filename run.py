from decouple import config
from app import create_app


config_name = config('APP_SETTINGS')
app = create_app(config_name)

if __name__ == '__main__':
    app.run()