import sys, csv
from decouple import config 
from app import create_app
from app.models import FeiraLivre


def export_csv_to_database(csv_path, test=False):
    """
    Read a csv File with the Feiras Livres and save them at
    the database.

    Can be executed at shell with the command
    'python export_csv.py <path_for_csv_file>'
    """
    try:
        if test:
            config_name = config('APP_TEST_SETTINGS')            
        else:
            config_name = config('APP_SETTINGS')
        app = create_app(config_name).app_context().push()
    except Exception as e:
        print("Can't find APP_SETTINGS =", e)
        raise      

    try:
        with open(csv_path, 'r') as csv_file:
            csv_reader = csv.DictReader(csv_file)
            line = {}
            for row in csv_reader:
                for key, value in row.items():
                    line[key.lower()] = value
                line.pop('id')
                line['numero'] = line.get('numero').split('.')[0]
                feira = FeiraLivre(**line)
                feira.save()
    except Exception as e:
        print(e)
        raise


if __name__ == '__main__':
    try:
        print('Exportando csv...')
        export_csv_to_database(sys.argv[1])        
        print('Csv exportado com sucesso...')        
    except Exception as e:
        print('Não foi possível exportar o Csv.')
